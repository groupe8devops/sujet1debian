#!/bin/bash

source /etc/apache2/envvars

# On installe des dépendences pour docker uniquement
[[ ! -e /.dockerenv ]] && exit 0

# xe pour developper plus rapidement en bash
set -xe

# On Installe les paquets necessaires, ici git par exemple, yqq va répondre yes automatiquement, et qq va cacher les messages
apt-get update -yqq
apt-get install git apt-utils -yqq
service apache2 start #On lance le service depuis ce script car la commande censer le faire sur Docker Freeze completement le conteneur

